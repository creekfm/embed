## Special classes

There are several convenient CSS classes applied at different times.

You can use them to show or hide things when desired. Or use them to apply special CSS styles when, for instance, the user is on a specific page.

#### body

- **creek-audio-toolbar-visible** &mdash;
- **creek-audio-has-show-image** &mdash;
- **creek-page-*** &mdash; Applied when the shows router activates a different page.
    ##### Pages include:
    - creek-page-shows
    - creek-page-show
    - creek-page-show-{show-short-name}
    - creek-page-schedule
    - creek-page-profile
    - creek-page-broadcast

#### #creek-audio-toolbar


# Methods

Creek.audio.play()

Creek.audio.pause()

Creek.audio.stop()

Creek.audio.switchToFile({title: "...", originURL: "..."})

Creek.audio.switchToLive(...)

Creek.audio.toggle()

Creek.audio.stream.setUrl(stream_url, restart = false)

Creek.audio.stream.setUrlFromStationApi(object: station API data from /api/station)

Creek.audio.analytics.startListening();

Creek.audio.analytics.stopListening();




## Audio Persistence

The embed library includes a basic script that helps you keep the audio playing even when users browse to different pages on your website.

It requires that you have a single, common container element for the main content.

1. When users click on links to browse the site, the script will intercept these link clicks.
1. Rather than changing the whole page, the intended page is loaded by AJAX.
1. The new content from the destination page is injected into the main container. Also, any elements with the `creek-sp-load` will also get refreshed.

### Example container element:

Here's a very simple example for what your container may look like. In this example, the container is `main-container`.

```
<div id="header">
  <div class="logo"><img src="/logo.png" /></div>
</div>
<div id="main-container">
  <h1>Heading for page</h1>
  <p>Content for page.</p>
  ...
  ...
</div>
<div class="footer">
  <p>Copyright &copy; 20XX Example Website</p>
</div>
```


### Updating other elements

Add the `creek-sp-load` class to all elements that should get updated when the page changes. For example, your website's navbar may change depending on the page that the user is on, so update this navbar along with the main container.

```
<div class="navbar creek-sp-load">
  <a href="/home">Home</a>
  <a href="/events">Events</a>
  <a href="/posts" class="active">Posts</a>
</div>
```


# Dependencies (the scripts in creek-lib.js)

Creek's embed library requires a few JavaScript components:

- jQuery
- Vue.js
- SoundManager2

These are bundled into the creek-lib.js file.


# Embed Content

## Shows

```
<div id="Creek-Shows"></div>
```

Add this tag to the main content area of a page to insert a gallery of your station's shows.

There should only be one instance of this tag on any one page.
