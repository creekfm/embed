
var $ = require('jquery');

module.exports.init = function(Creek){

  return {

    refresh: function(Creek){

      Creek.debug.log("Refreshing content.");

      //Send Google Analytics pageview
      if(typeof ga != 'undefined'){ ga('send', 'pageview', window.location); }

      //Reset Facebook plugins
      if(typeof FB != 'undefined') FB.XFBML.parse();

      //Reset SoundManager2
      if(typeof Creek.sm2_bar != 'undefined'){
        Creek.sm2_bar.replaceAudioTags();
        Creek.sm2_bar.build();
        Creek.sm2_bar.initiate(window);
      }

      //Reset inline image popup
      $(function(){
        //Check if it exists
        if($('.image-popup-vertical-fit').length)
          $('.image-popup-vertical-fit').magnificPopup({
            type: 'image'
            // other options
          });
      });

    }

  }

}
