/*

  Persistence: iframe

  - Keeps audio playing when users explore the website and move between pages.
  - Loads intended page content into a container element without changing the page.
  - Has a few callbacks for different stages in the request lifecycle.

*/

var $ = require('jquery');
var jQuery = $;
var htmldocFix = require('./jquery-htmldoc-fixer.js')(jQuery);

//persistInstance.historyData = [];


module.exports.init = function(Creek){

  //Return null early if not enabled or this is the wrong type
  if(!Creek.data.persist.enable || Creek.data.persist.type != "singlepage"){
    return null;
  }

  //Use settings for the selected type of persistence
  var persistSettings = Creek.data.persist.settings[Creek.data.persist.type];

  //HACK: Set up the "page changed" thing to fix the broken initial change
  // in history popstate.
  persistSettings.pageEverChanged = false;

  persistInstance = {};

  Creek.debug.log("Persistence domain: "+persistSettings.domain);
  Creek.debug.log("Persistence container: "+persistSettings.container);

  //prepare request var
  //(to prevent race condition if links are clicked quickly)
  //!!! wait, where is this code? what was this?

  persistInstance.loading = function(){

    persistInstance.onLoading();

    //Add loading class to body, to show a general "page loading" element
    //but add a timer so that the loading thing is only shown if slow, and check if it's already been loaded
    persistInstance.loadingStatus = true;
    setTimeout(function(){
      if(persistInstance.loadingStatus === true)
        $('body').addClass('creek-persistence-loading');
    }, persistInstance.loadingIndicatorDelay);

  }

  persistInstance.loaded = function(){

    persistInstance.onLoaded();

    persistInstance.loadingStatus = false;

    $('body').removeClass('creek-persistence-loading');

  }

  persistInstance.loadElementsWithClass = function(data){
    $('.creek-sp-load').map(function(){
      var id = $(this).attr('id')
      Creek.debug.log('id');
      var new_HTML_for_element = $(data).find('#'+id).html();
      $('#'+id).html( new_HTML_for_element );
    })
  }

  persistInstance.methods = {

    request_page: {
      abort: function(){}
    },

    handleContentData: function(x){

      persistInstance.loaded();

      var title = $(x.data).filter('title').text();

      Creek.debug.log(persistSettings.container);

      // var content = $(x.data).find(Creek.data.persistInstance.container).html();
      var content = $(x.data).find(persistSettings.container).html();
      // var content = $(x.data).filter(persistSettings.container).html();

      // console.log($(x.data).filter(persistSettings.container));
      // console.log($(x.data).filter('title'));
      // console.log($(x.data).filter('body'));
      // console.log($(x.data).find('body'));
      // console.log($(x.data).find('.navbar-wrapper'));
      // console.log($(x.data).find(persistSettings.container));
      // console.log($(x.data).find(persistSettings.container).html());

      // console.log(html.find('head').html());
      // console.log(html.find('head link').attr('href'));
      // console.log(html.find('head script').html());
      // console.log(html.find('head script').attr('src'));
      // console.log($(x.data).find('script').attr('src'));
      // console.log($(x.data).find('script'));
      // console.log(x.data)
      // console.log(x.data.indexOf("Creek_Main"))
      // console.log(x.data.indexOf("Creek_Main"))
      // console.log(x.data.indexOf("Creek_Main"))
      // console.log(htmlll)
      // console.log(scripts_in_head)

      Creek.debug.log("Content:");
      Creek.debug.log(content);
      Creek.debug.log("Title: "+title);

      // Apply all the new CSS/JS
      doTheNew = function(current_elements, new_elements, attr, tag){

        if(!persistSettings.loadNewAssets){
          return false;
        }

        /*
        console.log('NEW')
        new_elements.each(function(){  console.log($(this).attr(attr))  })

        console.log('OLD')
        current_elements.each(function(){  console.log($(this).attr(attr))  })

        */

        new_elements.each(function(){

          var its_new = true;

          var attr_new = $(this).attr(attr);
          // console.log(attr_new)


          if(!attr_new){
            // console.log('Not new.');
            return true;
          }


          //Go over current elements to find out if this new one already exists
          current_elements.each(function(key_n, v_n){

            var attr_current = $(v_n).attr(attr);

            //If it doesn't have an src or href, then skip it
            if(!attr_current) return true;

              // console.log(attr_current)
              // console.log(key_n)
              // console.log(v_n)

              // console.log('cccc: '+attr_current)
              // console.log('cccc: '+attr_new)

            if(attr_new == attr_current){
              its_new = false;
              // console.log('New.')
            }

          })

          if(its_new){

            if(tag == 'link')
              $('head').append('<link rel="stylesheet" href="'+attr_new+'" type="text/css" />');

            if(tag == 'script'){
              // $.getScript( attr_new );
              // $.getScript( attr_new, function( data, textStatus, jqxhr ) {
                // console.log( data ); // Data returned
                // console.log( textStatus ); // Success
                // console.log( jqxhr.status ); // 200
                // console.log( "Load was performed." );
              // });
              // $('head').append('<script type="text/javascript" src="'+attr+'"></script>');

              var js_html = '<script src="'+attr_new+'" type="text/javascript"></script>';

              // console.log(js_html)

              $('head').append(js_html);
              // $('head').append('<script src="'+attr+'" type="text/javascript"></script>');
              // console.log('woooff');
            }

            console.log('new: '+attr_new)

          }else{
            // console.log('old: '+attr_new)
          }


        })

      }


      // var htmll = x.data;
      var new_html = $.htmlDoc(x.data);


      //Apply new JS in head
      doTheNew($('head script'), new_html.find('head script'), 'src', 'script');

      //Apply new CSS in head
      doTheNew($('head link[type="text/css"]'), new_html.find('head link[type="text/css"]'), 'href', 'link');


      //Get scripts and execute them in order
      // var scripts_in_current_head = $('head script');
      // var scripts_in_new_head = new_html.find('head script');
      // var css_in_current_head = $('head link[type="text/css"]');
      // var css_in_new_head = new_html.find('head link[type="text/css"]');


      // console.log($(x.data).find('head').html());
      // console.log($(x.data).find('body').html());

      // console.log($('head').html())


      //Update window title with new page title
      window.document.title = title;

      //only push if link clicked
      if(x.push_state){
        window.history.pushState('', title, x.url);
        //persistInstance.historyData.push(x.data);
      }

      //Reset JS for:
      // Magnific Popup
      if(typeof $.magnificPopup != 'undefined') $.magnificPopup.close();

      //Scroll to top or scroll to scrollPos if defined (by pagination, maybe others later)
      if(persistInstance.scrollPos)
        $('html, body').scrollTop(persistInstance.scrollPos);
      else{
        //clear out content before scrolling up
        $(persistSettings.container).html(' ');
        $('html, body').scrollTop(0);
      }

      //Replace contents of #creek-sp-container
      $(persistSettings.container).html(content);

      //Also, replace contents of elements that are marked with .creek-sp-load class
      persistInstance.loadElementsWithClass(x.data);

      Creek.content.refresh(Creek);

      persistSettings.onComplete()

      //Run all of the functions in here
      if(typeof persistSettings.onCompleteArray !== "undefined"){
        for (var c = 0; c < persistSettings.onCompleteArray.length; c++) {
          persistSettings.onCompleteArray[c]();
        }
      }

      //alert(persistInstance.historyData[0]);

    },

    loadContent: function(x){

      console.log(x.url);

      //cancel previous request
      persistInstance.methods.request_page.abort();

      //alert(1);

      //Cover this in case it's not defined,
      // like when this method is called outside of this script.
      x.event = x.event || null;
      x.push_state = x.push_state || true;
      x.data = x.data || false;
      x.initial_history_action = x.initial_history_action || false;

      //prevent loading on page load
      if(x.event == null || x.event.state !== null || x.initial_history_action ){

        persistInstance.loading();

        //   ( v--- history handling)
        //If data not provided, then load it from URL
        // if(x.data == false)

          persistInstance.methods.request_page = $.get(x.url, function(data){

            x.data = data;

            persistInstance.methods.handleContentData(x);

          });

  /*
        //Else, use data (from history state pop)
        else{
          //alert(x.data);

          handleContentData(x);
        }
  */

      }

    },

    //Check if this URL points to a file (ex. PDF)
    checkIfFileUrl: function (url) {

      var ext = url.split('.').pop();
      var end_of_path = url.split('/').pop();

      Creek.debug.log("Extension: "+ext);
      Creek.debug.log("End of path: "+end_of_path);

      return (
        end_of_path.indexOf('.') !== -1
        && ext.length > 1
        && ext.length < 5
      );

    },

    //Check if this URL points to a file (ex. PDF)
    checkIfIgnorePaths: function (url) {

      var url_without_path = url.replace(url, window.location.pathname);

      Creek.debug.log("Checking ignore list: "+url);

      if(typeof persistInstance.ignorePaths != "undefined"){
        if(persistInstance.ignorePaths.indexOf(url_without_path) !== -1){
          Creek.debug.log("Ignoring: "+url_without_path);
          return true;
        }else {
          Creek.debug.log("Not ignoring: "+url_without_path);
          return false;
        }
      }else{
        return 0;
      }

    },

    checkLink: function (link, url) {

      // Creek.debug.log(url.indexOf("javascript:"))
      Creek.debug.log("Checking link's URL: "+url)

      if(

        url //has URL at all
        && (url.charAt(0) == '/' || url.indexOf(persistInstance.domain) != -1 ) //not external link
        && url.indexOf('/admin') !== 0 //not to admin
        && url.indexOf('/users/oauth_') !== 0 //not to oauth links (facebook sign-in)

        && !persistInstance.methods.checkIfFileUrl(url)

        && !persistInstance.methods.checkIfIgnorePaths(url)

        && !link.hasClass('logout-link') //not logout
        && !link.hasClass('no-js') //not no-js
        // url.indexOf('.') === -1 && //no dot (not a file)

        //WordPress
        && url.indexOf('wp-admin') == -1
        // && url.indexOf('/wp-admin') == -1
        && url.indexOf('/feed/') == -1
        // && url.indexOf('/comments/feed/') == -1
        && !link.closest("#wpadminbar").length
        && !link.closest(".ab-top-menu").length

      ){
        Creek.debug.log('Navigating to: '+url);
        return true;

      //Else, if an external link
      }else if(
        url.indexOf("javascript:") != -1 && //not javascript link
        url.charAt(0) != '#' //not within-page / anchor link
      ){
        // Creek.debug.log('Exiting to: '+url);
        Creek.debug.log('Letting link do its thing: '+url);
        return false;
      }
      //Otherwise do nothing
      // }else {
        // Creek.debug.log('Doing nothing with this link.');
      // }
    }

  }



  persistInstance.loadingBar = {
    start: function(){

      //wait for delay
      // var delay_timeout = setTimeout( function(){
        persistInstance.loadingBar.startAnimation();
      // }, 300);

      // persistInstance.loading.onStart(id);

    },
    end: function(){
      persistInstance.loadingBar.endAnimation();
      // persistInstance.loading.onDone(id);
    },
    startAnimation: function(){
      $('.creek-loading-bar')
        .addClass('creek-loading-bar-visible')
        .animate({'width': '50%'}, 300)
        .animate({'width': '80%'}, 300)
    },
    endAnimation: function(){
      $('.creek-loading-bar')
        .animate({'width': '100%'}, 150, function(){
          $(this)
            .removeClass('creek-loading-bar-visible')
            .css('width', '1%')
        })

    }
  }

  persistInstance.onLoading = function(){

    persistInstance.loadingBar.start()

  }
  persistInstance.onLoaded = function(){

    persistInstance.loadingBar.end()

  }

  //-----------------------------------------------
  // Actions to perform after page is loaded:
  //
  $(function(){
  // ^^^ (jQuery shorthand to wait for fully loaded DOM.)

    $(document).on('click', 'a', function(event){

      Creek.debug.log('Click!');

      persistSettings.pageEverChanged = true;

      //if (history.pushState) {

        var url = $(this).attr('href');

        //disable for: remote links, empty links, control panel links
        if(persistInstance.methods.checkLink($(this), url)){

          event.preventDefault();

          persistInstance.methods.loadContent({
            url:url, event:event,
            push_state:true,
            data: false
          });
          //loadContent({url:url, event:event, data: false});

        }

      //}

      /*
      */
      //Scroll to top of pagination element if pagination defined
      var scrollOffset = {};
      if($(this).closest('.pagination-container').length > 0 && $('.creek-pagination-scrollToHere').length > 0){
        scrollOffset = $('.creek-pagination-scrollToHere').offset();
        persistInstance.scrollPos = scrollOffset.top
      }

    });


    $(window).bind("popstate", function(event) {

      //var data = persistInstance.historyData.pop();
      //loadContent({url:window.location.pathname, event:event.originalEvent, push_state:false, data: data});

      Creek.debug.log("History event.");

      //HACK: Only invoke history if a page has been loaded
      //      during the user's session so far.
      if(persistSettings.pageEverChanged){

        Creek.debug.log("Travelling through history.");

        Creek.debug.log("Original event:");
        Creek.debug.log(event.originalEvent);

        persistInstance.methods.loadContent({
          url:window.location.pathname,
          event:event.originalEvent,
          push_state:false,
          initial_history_action:true,
          data: false
        });

      }
      else{
        Creek.debug.log("Not changing history. User hasn't changed pages yet.");
      }

    });

  });

  persistInstance.url = function(url){
    persistInstance.methods.loadContent({ url: url });
  }

  return persistInstance;

} //end export function container
