module.exports.init = function(config, Creek){

  var active = typeof config.debug != 'undefined' && config.debug;

  return {
    active: active,
    log: function(message){
      if(active){
        console.log(message);
      }
    }
  }

}
