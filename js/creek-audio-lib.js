
var $ = require('jquery');
var jQuery = $;
var soundManager = require('../node_modules/soundmanager2/script/soundmanager2-nodebug-jsmin.js').soundManager;


module.exports.init = function(Creek){

  return {

    // current: {
    // selected: {
    //   title: "Show title / audio file title",
    //   // type: "live",
    //   type: "file",
    //   Show: null,
    //   Track: null //normally null
    //   // Track: { ... }
    // },
    soundManager2_url: '',
    player: {}, //placeholder for SM2 object
    selected: null,
    state: null,
    current_player_id: null, //!!!! TODO: make this work
    html: {
      toolbar: {}
    },
    util: {},
    streamUrl: '',
    update: function(data){

      Creek.debug.log("Selecting audio:");
      Creek.debug.log(data);

      if(data.type == 'live'){

        //Set up the selected audio
        data.Show = Creek.data.broadcasting.now;
        data.Track = Creek.data.broadcasting.Track;
        Creek.audio.selected = data;

        Creek.debug.log('Updating live.');

        //
        //!!!!! JUST USE "AUDIO" INSTEAD? DECIDE ON THIS. !!!!!!
        //
        // //Update HTML elements
        // $('.creek-live-data .creek-show-title').html(data.title);
        // $('.creek-live-data a.creek-show-link').attr("href", data.originURL);

      }
      else if(data.type == 'file'){

        Creek.debug.log('Updating file.');

        //Set up the selected audio
        data.Show = null;
        data.Track = null;
        Creek.audio.selected = data;

      }
      else{
          Creek.debug.log('Warning: Unknown data type in update:', data);
      }

      //Update HTML elements
      $('.creek-live-data .creek-audio-title').html(data.title);
      $('.creek-live-data a.creek-audio-link').attr('href', data.originURL);

      //Display the image for the show.
      if(data.Show && data.Show.Image){
        var img_size = Creek.data.audio.toolbar.showImageSize;
        var show_url = Creek.data.shows.prefix+data.Show.short_name;
        $('.creek-audio-show-image img').attr("src", data.Show.Image["url_"+img_size]);
        $('.creek-audio-show-image').attr("href", show_url);
        $('body').addClass("creek-audio-has-show-image");
      }
      else{
        $('body').removeClass("creek-audio-has-show-image");
      }

      Creek.audio.updateTrack(data);

    },
    updateTrack: function(data){

      if(typeof data.Track != 'undefined' && data.Track && typeof data.Track.id != 'undefined'){

        $('.creek-live-data .creek-track-none').fadeOut(function(){

          $('.creek-live-data .creek-track-artist').html(data.Track.artist);
          $('.creek-live-data .creek-track-title').html(data.Track.title);

          //!!!!!!!!!!!!!!!!!!!!
          // !!! NOPE THIS AIN'T WERKING UNTIL I GET
          //     MY CDN DOWN IN THESE PARTS YO
          // ALBUM-ART !!!!!!!!!
          //
          // TODO:
          // - data.Track.lastfm_art >> data.Track.album_art
          // - remove PRP from buy button link below
          //
          // // $('.creek-buy-track-button').attr('href', 'http://prpfm.creek.fm/tracks/buy/'+data.Track.id);
          // // alert(data.Track.lastfm_art);
          // var image_url = lastfm_art_url + data.Track.lastfm_art;

          // //Show album art if available
          // if(data.Track.lastfm_art && (album_art_previous != data.Track.lastfm_art || !album_art_previous ))
          //   $('.creek-live-data .creek-album-art').html('<img src="'+image_url+'" />');
          // // else

          // //Hide image if not provided, or if it's missing/404
          // // if(!data.Track.lastfm_art || !Creek.util.images.imageExists(image_url)) $('.creek-live-data .creek-album-art').html('');
          // if(!data.Track.lastfm_art)
          //   $('.creek-live-data .creek-album-art').html('');

          //Last, make sure it's faded in, once data in is place.
          $('.creek-live-data .creek-track, .creek-live-data .creek-track-album-art').fadeIn(function(){})

          // album_art_previous = data.Track.lastfm_art;
          //
          // ALBUM-ART !!!!!!!!!
          //!!!!!!!!!!!!!!!!!!!!


          // Creek.debug.log(data.Track.artist);

        });

      }
      else{
        $('.creek-live-data .creek-track, .creek-live-data .creek-track-album-art').fadeOut(500, function(){
            $('.creek-live-data .creek-track-none').fadeIn();
        });
      }

    },

    setState: {

      // This state is active while SM2 is loading
      initializing: function(){

          Creek.audio.state = "initializing";

          Creek.debug.log("Audio state: initializing.");

          $("body").addClass('creek-audio-initializing');

      },

      // SM2 ready (onready() event fired)
      initialized: function(){

          Creek.audio.state = "initialized";

          Creek.debug.log("Audio state: initialized.");

          $("body").removeClass('creek-audio-initializing');

      },

      playing: function(){

        Creek.audio.state = "playing";

        Creek.debug.log("Audio state: playing.");

        $("body").addClass('creek-audio-playing');
        $("body").removeClass('creek-audio-loading');
        $("body").removeClass('creek-audio-error');

        //Switch classes on the button
        $(".creek-audio-main-button")
          .removeClass('creek-audio-play-button')
          .addClass('creek-audio-pause-button')
          .removeClass('creek-audio-loading-button');

      },
      paused: function(){

        Creek.audio.state = "paused";

        Creek.debug.log("Audio state: paused.");

        $("body").removeClass('creek-audio-playing');
        $("body").removeClass('creek-audio-loading');
        $("body").removeClass('creek-audio-error');

        //Switch classes on the button
        $(".creek-audio-main-button")
          .addClass('creek-audio-play-button')
          .removeClass('creek-audio-pause-button')
          .removeClass('creek-audio-loading-button');

      },
      // SM2 Sound object is loading/buffering
      loading: function(){

        Creek.audio.state = "loading";

        Creek.debug.log("Audio state: loading.");

        $("body").addClass('creek-audio-loading');
        $("body").removeClass('creek-audio-playing');
        $("body").removeClass('creek-audio-error');

        //Switch classes on the button
        $(".creek-audio-main-button")
          .removeClass('creek-audio-play-button')
          .removeClass('creek-audio-pause-button')
          .addClass('creek-audio-loading-button');

      },
      stopped: function(){

        //Just use the paused state again.
        //TODO: actually have a stop state?
        Creek.audio.setState.paused();

        Creek.debug.log("Audio state: stopped.");

        //!!! Put here since pause updates state as well:
        Creek.audio.state = "playing";

      },
      error: function(){

        $("body").removeClass('creek-audio-playing');
        $("body").removeClass('creek-audio-loading');
        $("body").addClass('creek-audio-error');

        Creek.audio.state = "playing";

        Creek.debug.log("Audio state: error.");

      },

    },

    switchToLive: function(data){

      Creek.debug.log('Switching to live.');

      //Set up broadcasting-based data
      if(Creek.data.broadcasting.now){
        var stream_title = Creek.data.broadcasting.now.title;
        var show_url = Creek.data.shows.prefix + Creek.data.broadcasting.now.short_name || "#"
      }
      else{
        var stream_title = Creek.data.station.station_name;
      }

      //Set up title
      var title = data.title || stream_title;
      var originURL = data.originURL || show_url;

      // console.log("BOINK");
      // console.log(originURL);
      // console.log(showURL);
      // console.log(originURL);

      soundManager.stopAll();

      var data_selected = {
        type: "live",
        title: title,
        originURL: originURL,
        Show: Creek.data.broadcasting.now,
        Track: Creek.data.broadcasting.Track
      };

      //Update classes on body
      $("body").addClass('creek-audio-mode-live');
      $("body").removeClass('creek-audio-mode-file');

      Creek.audio.selected = data_selected;

      Creek.debug.log(Creek.audio.selected);

      Creek.audio.update(data_selected);

      Creek.audio.setState.stopped();

      $('.creek-audio-switch-to-live').addClass('creek-hide');

    },

    activateInitialLiveAudio: function(Creek){

      var audio_data = {
        type: "live",
        title: "",
        originURL: "#"
      };

      Creek.audio.selected = audio_data;

      Creek.audio.util.resetSpacerHeight();

    },
    
    showAudioToolbar: function(Creek){

      //Show the audio toolbar
      var tbv = "creek-audio-toolbar-visible";
      if(Creek.data.audio.toolbar.enable && !$('body').hasClass(tbv)){
        Creek.debug.log("Toolbar enabled.")
        $(function(){
          $('body').addClass(tbv);
        });
      }

    },

    switchToFile: function(data){

      var title = data.title || Creek.data.station.title;
      var originURL = data.originURL || "#";

      Creek.audio.update({
        type:"file",
        title: title,
        originURL: originURL
      });

      //Update classes on body
      $("body").addClass('creek-audio-mode-file');
      $("body").removeClass('creek-audio-mode-live');

      Creek.audio.setState.stopped();

      $('.creek-audio-switch-to-live').removeClass('creek-hide');

    },

    util: {

      resetSpacerHeight: function(){
        var h = $('#creek-audio-toolbar').outerHeight() + "px";
        $("#creek-audio-toolbar-spacer").css('height', h);
        $(".creek-audio-toolbar-spacer").css('height', h);
      }

    },

    analytics: {

      startListening: function(){

        //Send Google Analytics event (start)
        if(typeof ga != 'undefined'){
          ga('send', 'event', 'button', 'play', 'stream player');
        }

        //Start Google Analytics event for listening
        Creek.listenTimer = setInterval(function(){
          if(typeof ga != 'undefined'){
            ga('send', 'event', 'listen', Creek.audio.selected.type, Creek.audio.selected.title, 1);
          }
        }, 60000);

        // Creek.debug.log("logging!")
        // console.log(Creek.audio.selected);

      },

      stopListening: function(){

        //Send Google Analytics event (start)
        if(typeof ga != 'undefined'){
          ga('send', 'event', 'button', 'play', 'stream player');
        }

        //Start Google Analytics event for listening
        clearInterval(Creek.listenTimer);

      }


    }

  }

}
