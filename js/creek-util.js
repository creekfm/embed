/*

Utilities for optional functions.

*/


var $ = require('jquery');
var jQuery = $;


module.exports.init = function(Creek){

  //!!! TODO: this should be somewhere else.
  //Add the loading bar container to the page...
  $(function(){
    $('body').append('<div class="creek-loading-bar">&nbsp;</div>');
  });

  var util = {};

  util.fixLinks = function(html){

    // console.log(html);

    //If not enabled, then return HTML unedited
    if(!Creek.data.router.enable){
      return html;
    }

    var replace = function(str, find, replace){

      //Replace all occurrences of this link prefix
      var str = str.replace(new RegExp('href="'+find, 'g'), 'href="'+replace);

      //Replace all the ones with single quotes in Vue template inline JS
      var str = str.replace(new RegExp('href="\''+find, 'g'), 'href="\''+replace);

      return str;
    }

    for(find in Creek.data.router.linkReplacements){
      html = replace(html, find, Creek.data.router.linkReplacements[find]);
    }

    return html;

  }


  //Cookie functions for the preroll
  // These are used to make sure the preroll only fires
  // once every 7 minutes.
  util.cookies = {};
  util.cookies.create = function(name,value,seconds) {
    if (seconds) {
      var date = new Date();
      date.setTime(date.getTime()+(seconds));
      var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
  };
  util.cookies.read = function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
  };
  util.cookies.delete = function(name) {
    Cookies.create(name,"",-1);
  };

  //Images
  util.images = {

    imageExists: function(image_url){

      var http = new XMLHttpRequest();

      http.open('HEAD', image_url, false);
      http.send();

      return http.status != 404;

    }

  }

  util.capitalizeFirstLetter = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  return util;

}
