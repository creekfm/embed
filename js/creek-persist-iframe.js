/*

  Persistence: iframe

  - Keeps audio playing when users explore the website and move between pages.
  - Loads page content dynamically, without actually changing the page.
  - Has a few callback options for different stage in the request lifecycle.

*/

var $ = require('jquery');
var jQuery = $;

//persistInstance.historyData = [];


module.exports.init = function(Creek){

  //Return null early if not enabled or this is the wrong type
  if(!Creek.data.persist.enable || Creek.data.persist.type != "iframe"){
    return null;
  }

  //Use settings for the selected type of persistence
  var persistSettings = Creek.data.persist.settings[Creek.data.persist.type];

  Creek.debug.log("Persistence domain: "+persistSettings.domain, 3);

  persistInstance = {};

  //prepare request var
  //(to prevent race condition if links are clicked quickly)
  //!!! wait, where is this code? what was this?

  /*

  iFrame-based persistence

  Docs:

  Use the `.creek-persist` class to make elements stay in the same (parent) scope, rather than only inside the `<iframe>`.

  Submit forms to parent frame by adding `target="_parent"` to your `<form>` opening tag. As a fallback, this will submit to the current frame if it's not inside an iframe.


  */

  /*

  - Vue methods for channel selector should access the methods of the parent frame.
  - If channel changed, then start playing audio for that channel.

  */

  persistInstance.url = function(url){
    window.location = url;
  }

  persistInstance.checkFirefox = function(){
    return navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
  }

  persistInstance.checkInternetExplorer = function() {

    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // Edge (IE 12+) => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;

  }


  persistInstance.checkIfBrowserSupport = function(){

    if(
      //Not Firefox
      !persistInstance.checkFirefox()
      //Not IE
      && !persistInstance.checkInternetExplorer()
    ){
      return true;
    }
    else{
      return false;
    }

  }
  persistInstance.insideIframe = function(){

    if(
      //Not already inside an iframe
      window.frameElement
    ){
      Creek.debug.log("Persistence: Yep, already inside an iframe.", 3);
      return true;
    }
    else{
      Creek.debug.log("Persistence: Not already inside iframe.", 3);
      return false;
    }

  }

  persistInstance.createMagicIframe = function(){

    //Create iframe with the current URL
    // ...but only if it's not already inside an iframe,
    //    and other important conditions (see above).
    if (
      persistInstance.checkIfBrowserSupport()
    ) {

      if(!persistInstance.insideIframe()){

        $("body").addClass("creek-persist-reset-body-css");

        Creek.debug.log("Persistence: Conjuring up magic frame.", 3);

        //Hide everything on the page, in the body, except for a few special elements.
        $("body *").not('#creek-audio-toolbar, #creek-audio-toolbar *, .creek-persist, .creek-persist *').remove();

        /*
        Add the magic iframe to the body that contains the current page without modification.

        Notice that it includes a few special attributes:

        - It's 100% width and height, with no border, so that it fills the entire window and isn't discernable as anything but a regular, non-iframed page.

        - Whenever its page URL/location changes (the "onLoad" event) it calls a function that updates the parent URL of the window. Otherwise the browser's URL bar look "stuck" when you change pages.

        */
        $("body").prepend('<iframe id="creek-persist-audio-iframe" src="'+window.location+'" style="position:absolute; height:100%; width: 100%; z-index: -1; border:0; height: 1px; top:0; left:0; min-height: 100%; -webkit-overflow-scrolling: touch;" onLoad="Creek.data.persist.instance.updateCurrentURLFromIframe(this.contentWindow.location);"></iframe>')

        Creek.debug.log("Persistence: Showing audio toolbar.");

        //Display the audio toolbar:
        $('#creek-audio-toolbar')
          .css("display", "block")
          .removeClass("creek-hide");

      }
      //Otherwise, don't initiate magic iframe and just hide the audio toolbar.
      else{

        Creek.debug.log("Persistence: Hiding audio toolbar.");
        $('#creek-audio-toolbar')
          .css("display", "none")
          .addClass("creek-hide")
          .addClass("yeahdude");

      }

    }else{

      Creek.debug.log("Persistence: Browser doesn't support iframe persistence!", 3);

      //Show the audio toolbar, but only if it's not already inside an iframe
      if(persistInstance.insideIframe()){
        Creek.debug.log("Hiding audio toolbar.");
        $('#creek-audio-toolbar')
          .css("display", "none")
          .addClass("creek-hide");
      }
      else{
        $('#creek-audio-toolbar')
          .css("display", "block")
          .removeClass("creek-hide");
      }

    }

  }

  //Updates the parent window's URL so that it matches the iframe
  persistInstance.updateCurrentURLFromIframe = function(url){

    //Update page title
    var title = document.getElementById("creek-persist-audio-iframe").contentDocument.title;
    document.title = title;

    //Update URL
    // window.history.pushState("yeah", title, url);
    window.history.replaceState("Page", title, url);

  }

  //Change the iframe's URL
  persistInstance.updateIframeURL = function(url){
    document.getElementById("creek-persist-audio-iframe").src = url;
  }

  // Creek.togglePlayFromIFrame = function(url){

  //   //Update page title
  //   var title = document.getElementById("creek-persist-audio-iframe").contentDocument.title;
  //   document.title = title;

  //   //Update URL
  //   // window.history.pushState("yeah", title, url);
  //   window.history.replaceState("Page", title, url);

  // }

  //Make all of the links that point to other domains change the parent window URL.

  //...


  //Do things that should happen after page load.
  $(function(){

    Creek.debug.log("hello", 3);

    //Create the magic iframe that contains the actual website content.
    persistInstance.createMagicIframe();

    //Links inside audio toolbar and creek-persist eleemnts should update the iframe
    // These links have the ".creek-persist-link" class
    $(document).on("click", ".creek-persist-link", function(event){
      console.log("Persist link clicked.")
      var url = $(this).attr("href");
      Creek.updateIframeURL(url);
      event.preventDefault();
      return false;
    })

  })


  return persistInstance;

} //end export function container
