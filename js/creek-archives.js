
// console.log("YEAHHHH");

//-------------------------------------------------------
// ARCHIVES
//-------------------------------------------------------

var $ = require('jquery');

module.exports.init = function(Creek, Vue){

  // var moment = require('moment');

  //---------------------------
  // Vue, for rendering the templates.
  //---------------------------
  // var Vue = require('vue');
  //
  // if(Creek.debug.active){
  //   Vue.config.debug = true;
  // }
  //
  // //Add Vue Moment filter
  // Vue = require('./vue-moment.js')(Vue, moment);
  //
  // // var VueRouter = require('vue-router')
  //
  // Vue.use(VueRouter)

  var initialLoad = function(callback){

    Creek.debug.log("Opening archives pages.");

    getBroadcasts({}, function(data){
    // $.getJSON(url, function(data){
      console.log(data);
      // Creek.debug.log(data);
      // data.html = data;
      // Creek.debug.log(data);
      // setTitle(data.title);
      return callback(data);
    // })
    })

  }

  function getBroadcasts(x, callback){

    console.log("fuck");
    // console.log(url);

    var callback = callback || function(){}

    //Set up data for API request
    var x = x || {};
    var input_limit = x.limit || 20;
    var input_offset = x.offset || 0;
    var input_term = $("#creek-broadcasts-search-box").val() || '';
    var input_show = $("#creek-broadcasts-program").val() || '';
    var input_show_category = $("#creek-broadcasts-show-category").val() || '';
    var input_date_start = $("#creek-broadcasts-date-start").val() || '';
    var input_date_end = '';

    if(input_date_start){
      input_date_end = input_date_start;
      // input_date_end = moment(input_date_start+' 00:00:00').add(25, 'hours').format('YYYY-MM-DD');
    }

    //Add the filters
    var url = Creek.data.url_prefix+"/api/broadcasts?x=1";
    url += "&limit="+input_limit;
    url += "&offset="+input_offset;
    url += "&search="+input_term;
    url += "&show_category="+input_show_category;
    url += "&date_start="+input_date_start;
    url += "&date_end="+input_date_end;
    url += "&cr=hide_expired" //hide copyright-expired broadcasts

    if(input_show){
      url += "&show="+input_show;
    }

    //Add JSONP callback
    url += "&callback=?";


    $.getJSON(url, function(data){
      // components.archives.broadcasts = data;
      // console.log(data);
      $("#creek-broadcasts").removeClass("invisible");
      callback(data);
    });

  }


  $Archives = Vue.component('creek-archives', {
    // template: require('./templates/archives'),
    template: Creek.util.fixLinks(require('./templates/archives')),
    data: function(){
      console.log("data worked")
      return {
        broadcasts:{},
        programs:{},
        categories:{}
      }
    },
    route: {
      data: function(transition){


        // var url = Creek.data.url_prefix+"/shows/"+transition.to.params.short_name+"?external=1&external_complete=1&callback=?";
        // Creek.debug.log(url);

        console.log("loading for archives");

        initialLoad(function(data){
          console.log("yeps");
          var data2 = { broadcasts: data};
          // $Archives.broadcasts = data;
          transition.next(data2);
          // swapPageClass("creek-page-archives"); //!!! add this again
        });

        // $.getJSON(Creek.data.url_prefix+"/api/shows?callback=?", function(data){
        //   console.log(data);
        //   console.log("fuck")
        //   this.$data.programs = data;
        // });
        //
        // $.getJSON(Creek.data.url_prefix+"/api/program_categories?callback=?", function(data){
        //   console.log(data);
        //   console.log("fuckk")
        //   this.$data.categories = data;
        // });

      }
    }
  })

  // var App = Vue.extend({})
  // var router = new VueRouter()
  // var router = new VueRouter({saveScrollPosition: true})
  // var router = new VueRouter({saveScrollPosition: true, history: true})

  // router.map({
  //   '/archives': {
  //     component: components.Archives
  //   }
  // })


  // var render = function(){
  //
  //   console.log("yess")
  //
  //   var container = Creek.data.shows.container;
  //
  //   //Check for container on the page.
  //   if($(container).length < 1){
  //     return;
  //     Creek.debug.log("Creek: No container element for content.");
  //   }
  //
  //   //Add the router view outlet/container for Vue Router
  //   // $(container).html("<router-view></router-view>");
  //
  //   // router.start(App, container);
  //
  // }

  //
  // var container = '#Creek-Archives';
  //
  // //If container is found, then use it.
  // if($(container).length > 0){
  //
  //   $(container).html(html_archives);



    // getBroadcasts();

    $("body").on("click", "#broadcasts-search-button", function(){
      getBroadcasts();
    })

    // $("#archives-search-form input, #archives-search-form select").on("input", function(){
    //   getBroadcasts();
    // })

    // var pickadate_settings = {
    //
    //   // Editable input
    //   // editable: true,
    //
    //   format: 'yyyy-mm-dd',
    //
    //   // Dropdown selectors
    //   selectYears: true,
    //   selectMonths: true,
    //
    //   weekdaysShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    //
    //   onSet: function(context) {
    //     // getBroadcasts();
    //   }
    //
    // }
    //
    // var broadcasts_date_start = $('#broadcasts-date-start').pickadate(pickadate_settings)
    // var broadcasts_date_end = $('#broadcasts-date-end').pickadate(pickadate_settings)

  // }


  //Run on first page load
  // $(function(){
  //   render();
  // })

  return $Archives;

}
