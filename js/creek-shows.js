/*

Routes:

Loads Creek station content with a front-end JS router.

*/

module.exports.init = function(Creek){

  var $ = require('jquery');

  var moment = require('moment');

  //---------------------------
  // Vue, for rendering the templates.
  //---------------------------
  var Vue = require('vue');

  if(Creek.debug.active){
    Vue.config.debug = true;
  }

  var url_prefix = "https://"+Creek.data.station.domain;
  // var url_prefix = Creek.data.schema+"://"+Creek.data.station.domain;

  //Add Vue Moment filter
  Vue = require('./vue-moment.js')(Vue, moment);


  var VueRouter = require('vue-router')

  Vue.use(VueRouter)

  //For the search bar on Archives page.
  Vue.filter('notEmpty', function (value, input) {
    // here `input` === `this.userInput`
    return value + input
  })

  /* ------------------------------------------------------

  Set up routing for the schedule.

  */

  // Define some components
  var components = {};

  var setTitle = function(title){

    //Disable if disabled.
    if(!Creek.data.router.enableTitles){
      return;
    }

    if(Creek.data.station.title){
      var append = " - "+Creek.data.station.title;
    }
    else{
      var append = "";
    }

    $("head title").html(title+append);

  }

  var swapPageClass = function(page){

    $("body").removeClass (function (index, css) {
      return (css.match (/(^|\s)creek-page-\S+/g) || []).join(' ');
    });
    $("body").addClass("creek-page-"+page)

  }

  components.Show = Vue.component('creek-show', {
    template: Creek.util.fixLinks(require('./templates/show')),
    data: function(){ return { html: '', title: ''} },
    route: {
      data: function (transition) {
        Creek.debug.log("Received data for Show.");
        $.getJSON(url_prefix+"/shows/"+transition.to.params.short_name+"?external=1&external_complete=1&callback=?", function(data){
          data.html = Creek.util.fixLinks(data.html);
          // Creek.debug.log(data);
          setTitle(data.title);
          transition.next(data)
        })
        swapPageClass("show creek-page-show-"+transition.to.params.short_name)
      }
    }
  })

  //Called by route: /shows/*
  var getShows = function(weekday_number, data){

    // console.log(data);

    var filteredData = {
      shows: data[weekday_number].programs
    }

    $(".creek-weekday").removeClass("selected");
    $(".creek-weekday-"+weekday_number).addClass("selected");

    // Creek.debug.log("Filtered data for schedule:");
    // Creek.debug.log(filteredData);

    return filteredData;

  }


  components.Shows = Vue.component('creek-shows', {
    template:  Creek.util.fixLinks(require('./templates/shows')(Creek)),
    data: function(){
      return {
        shows: []
      }
    },
    route: {
      data: function (transition) {

        //Get weekday
        if(typeof transition.to.params.weekday_number != 'undefined'){
          var weekday_number = transition.to.params.weekday_number
        }
        else{
          var weekday_number = moment().format('E');
        }

        //Get data from schedule if it's already been retrieved
        if(Creek.data.shows.data !== null){
          var data = getShows(weekday_number, Creek.data.shows.data);
          transition.next(data);
        }
        else {
          $.getJSON(url_prefix+"/api/schedule?callback=?", function(data){
            Creek.data.shows.data = data;
            transition.next(getShows(weekday_number, data));
          });
        }

        setTitle(Creek.util.capitalizeFirstLetter(Creek.data.router.showsName));
        // setTitle(Creek.data.shows.showsName);

        swapPageClass("shows");
      }
    }
  })

  components.Broadcast = Vue.component('creek-broadcast', {
    template: Creek.util.fixLinks(require('./templates/content-page')(Creek)),
    data: function(){ return { html: '', title: ''} },
    route: {
      data: function (transition) {
        $.getJSON(url_prefix+"/broadcasts/"+ transition.to.params.broadcast_id+"?json_wrap=1&external=1&external_complete=1&callback=?", function(data){

          data.html = Creek.util.fixLinks(data.html, "broadcast");
          // Creek.debug.log(data);
          setTitle(data.title);
          swapPageClass("broadcast");
          transition.next(data);

          Creek.content.refresh(Creek);

          // //!!! This is not reliable since it relies on time.
          // setTimeout(function(){
          //   Creek.content.refresh(Creek);
          // }, 100);

          //!!! This is not reliable since it relies on time.
          setTimeout(function(){
            Creek.content.refresh(Creek);
          }, 500);

          // //!!! This is not reliable since it relies on time.
          // setTimeout(function(){
          //   Creek.content.refresh(Creek);
          // }, 1000);

        })
      },
      activate: function(transition){

        transition.next();

        // Creek.content.refresh(Creek);

      }
    }
  })

  components.Profile = Vue.component('creek-profile', {
    template: Creek.util.fixLinks(require('./templates/content-page')(Creek)),
    data: function(){ return { html: '', title: ''} },
    route: {
      data: function (transition) {
        $.getJSON(url_prefix+"/profiles/"+transition.to.params.username+"?json_wrap=1&external=1&external_complete=1&callback=?", function(data){
          data.html = Creek.util.fixLinks(data.html, "profile");
          // Creek.debug.log(data);
          setTitle(data.title);
          swapPageClass("profile");
          transition.next(data);
        })
      }
    }
  })

  components.Schedule = Vue.component('creek-schedule', {
    template: Creek.util.fixLinks(require('./templates/schedule')),
    data: function(){ return { html: '', title: ''} },
    route: {
      data: function (transition) {
        //Get data from schedule if it's already been retrieved
        if(Creek.data.shows.schedule.data !== null){
          transition.next(Creek.data.shows.schedule.data);
        }
        else {
          $.getJSON(url_prefix+"/shows/schedule?json_wrap=1&external=1&external_complete=1&callback=?", function(data){
            data.html = Creek.util.fixLinks(data.html)
            Creek.data.shows.schedule.data = data;
            transition.next(data);
          });
        }
        setTitle("Schedule");
        swapPageClass("schedule");
      }
    }
  })

  components.ShowsAll = Vue.component('creek-shows-all', {
    template: Creek.util.fixLinks(require('./templates/shows-all')),
    data: function(){
      return {
        shows: [],
        findTerm: ''
      }
    },
    route: {
      data: function (transition) {
        //Get data from schedule if it's already been retrieved
        if(Creek.data.shows.all.data !== null){
          transition.next({shows: Creek.data.shows.all.data});
        }
        else {
          $.getJSON(url_prefix+"/api/shows?callback=?", function(data){
            Creek.data.shows.all.data = data;
            transition.next({shows: Creek.data.shows.all.data});
          });
        }
        setTitle("Schedule");
        swapPageClass("schedule");
      }
    }
  })

  var App = Vue.extend({})
  // var router = new VueRouter()
  var router = new VueRouter({saveScrollPosition: true})
  // var router = new VueRouter({saveScrollPosition: true, history: true})

  router.map({
    '/': {
      component: components.Shows
    },
    '/weekday/:weekday_number': {
      component: components.Shows
    },
    '/profiles/:username': {
      component: components.Profile
    },
    '/broadcasts/:broadcast_id': {
      component: components.Broadcast
    },
    '/schedule': {
      component: components.Schedule
    },
    '/all': {
      component: components.ShowsAll
    },
    '/shows/:short_name': {
      component: components.Show
    },
    '/archives': {
      component: require('./creek-archives').init(Creek, Vue)
    },
    '/play': { },
    // '/:whatever': {
    //   component: null
    // },
    '/:short_name': {
      component: components.Show
    },
  })


  var render = function(){

    var container = Creek.data.shows.container;

    //Check for container on the page.
    if($(container).length < 1){
      return;
      Creek.debug.log("No container element for shows.");
    }

    //Add the router view outlet/container for Vue Router
    $(container).html("<router-view></router-view>");

    router.start(App, container);

  }

  //When hovering on grouped show titles, move their respective images to be the first element in the image group container, so that CSS can show them
  $(document).on("hover", ".creek-show-group-title-link", function(){

    //Set up vars
    var show_id = $(this).attr("data-show-id");
    var show_element = $(this).closest(".creek-show")
    var image_group = show_element.find(".creek-grouped-image-container")
    var grouped_image_element = image_group.find(".creek-grouped-image-"+show_id);

    //Move the element
    image_group.prepend(grouped_image_element);

  })

  //Handle running this when page loads, in either Creek basicPersistence or regular page load

  if(typeof Creek.data.persist.settings.singlepage.onCompleteArray){
    Creek.data.persist.settings.singlepage.onCompleteArray.push(render);
  }

  //Run on first page load
  $(function(){
    render();
  })

}
