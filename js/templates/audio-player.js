module.exports = function(audio_files, dark_text){

  function build_audio_rows(audio_files){

    var audio_files_html = '';

    for (var i = 0; i < audio_files.length; i++) {
      audio_files_html +=
       '<li><a oncontextmenu="return false" class="player" href="'+audio_files[i].url+'">'+audio_files[i].title+'</a></li>';
    }

    // console.log(audio_files_html);

    return audio_files_html;

  };

  //Option: Dark text
  dark_text_class = "";
  if(dark_text){
    dark_text_class = " dark-text";
  }

  //Only show the playlist if there is more than one audio file.
  var playlist_open = audio_files.length > 1 ? " playlist_open" : "";

  var html =
   '<div class="sm2-bar-ui flat full-width'+playlist_open+dark_text_class+'">'
  +'  <div class="bd sm2-main-controls">'
  +'    <div class="sm2-inline-texture"></div>'
  +'    <div class="sm2-inline-gradient"></div>'
  +'    <div class="sm2-inline-element sm2-button-element">'
  +'      <div class="sm2-button-bd">'
  +'        <a href="#play" class="sm2-inline-button play-pause">Play / pause</a>'
  +'      </div>'
  +'    </div>'
  +'    <div class="sm2-inline-element sm2-inline-status">'
  +'      <div class="sm2-playlist">'
  +'        <div class="sm2-playlist-target">'
  +'          <ul class="sm2-playlist-bd">'
  +'            <li></li>'
  +'          </ul>'
  +'        </div>'
  +'      </div>'
  +'      <div class="sm2-progress">'
  +'        <div class="sm2-row">'
  +'          <div class="sm2-inline-time">0:00</div>'
  +'          <div class="sm2-progress-bd">'
  +'            <div class="sm2-progress-track">'
  +'              <div class="sm2-progress-bar" style="width: 0.0986938%;"></div>'
  +'              <div class="sm2-loading-bar"></div>'
  +'              <div class="sm2-progress-ball" style="left: 0.0986938%;">'
  +'                <div class="icon-overlay"></div>'
  +'              </div>'
  +'            </div>'
  +'          </div>'
  +'          <div class="sm2-inline-duration"></div>'
  +'        </div>'
  +'      </div>'
  +'    </div>'
  +'    <div class="sm2-inline-element sm2-button-element sm2-volume">'
  +'      <div class="sm2-button-bd">'
  +'        <span class="sm2-inline-button sm2-volume-control volume-shade"></span>'
  // +'        <br>'
  +'        <a href="#volume" class="sm2-inline-button sm2-volume-control">volume</a>'
  +'      </div>'
  +'    </div>'
  +'  </div>'
  +'  <div class="bd sm2-playlist-drawer sm2-element">'
  +'    <div class="sm2-inline-texture">'
  +'      <div class="sm2-box-shadow"></div>'
  +'    </div>'
  +'    <div class="sm2-playlist-wrapper">'
  +'      <ul class="sm2-playlist-bd">'

  + build_audio_rows(audio_files)

  +'      </ul>'
  +'    </div>'
  +'    <div class="sm2-extra-controls">'
  +'      <div class="bd">'
  +'        <div class="sm2-inline-element sm2-button-element">'
  +'          <a href="#prev" title="Previous" class="sm2-inline-button previous">&lt; previous</a>'
  +'        </div>'
  +'        <div class="sm2-inline-element sm2-button-element">'
  +'          <a href="#next" title="Next" class="sm2-inline-button next">&gt; next</a>'
  +'        </div>'
  +'      </div>'
  +'    </div>'
  +'  </div>'
  +'</div>';

  //Remove linebreaks from the output HTML
  // WordPress: This specifically fixes the automatic "<p></p>" injection.
  var html_filtered = html.replace(/(\r\n|\n|\r)/gm,"");

  return html_filtered;

}
