module.exports = function(Creek){

  var html = ''
  + '<div v-if="$loadingRouteData">'+Creek.data.router.loadingHtml+'</div>'
  + '<div v-if="!$loadingRouteData" v-html="$data.html"></div>'

  // Creek.debug.log(html);
  // Creek.debug.log(html);

  return html;

}
