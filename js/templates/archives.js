// module.exports =

// // ' <div id="Archive">'+
// // '  <div class="container">'+
// // '    <h1>Search the Archives</h1>'+
// // '  </div>'+
// '        <div class="creek-broadcast" v-for="broadcast in broadcasts" :data-url= \'/broadcasts/\'+broadcast.id">'+
// '            <span class="creek-image"><img v-attr="src: broadcast.Image ? broadcast.Image.url_sm : broadcast.Show.Image.url_sm" /></span>'+
// '            <h3><span class="archive-title"><span v-text="broadcast.Show.title"></span> - <span v-text="broadcast.title"></span></span></h3>'+
// '            <h3><span class="creek-archive-title"><span v-text="broadcast.Show.title"></span></span></h3>'+
// '            <div class="creek-item-info">'+
// // '              <!-- <a class="show-title" v-text="broadcast.Show.title" v-attr="href: \'/broadcasts/view/\'+broadcast.id"></a> -->'+
// '              <span class="creek-date-time" v-text="broadcast.start | moment \'dddd, MMMM Do YYYY, h:mma\'"></span>'+
// '            </div>'+
// '            <div class="creek-item-description">'+
// '              <!-- <a class="read-more" v-attr="href: \'/broadcasts/\'+broadcast.id ">Read more</a> -->'+
// '              <span v-text="broadcast.text"></span>'+
// '            </div>'+
// '            <div class="creek-social-buttons">'+
// '              <span class="creek-social-button creek-social-button-facebook" v-attr="data-url: Creek.base_url+\'/broadcasts/\'+broadcast.id " href="#"><i class="fa fa-facebook"></i> Share</span>'+
// '              <span class="creek-social-button creek-social-button-twitter" v-attr="data-url: Creek.base_url+\'/broadcasts/\'+broadcast.id " href="#"><i class="fa fa-twitter"></i> Tweet</span>'+
// '              <span class="creek-social-button creek-social-button-email" href="#" v-attr="data-url: Creek.base_url"><i class="fa fa-envelope"></i> Email</span>'+
// '            </div>'+
// '        </div>'+
// // '</div>';
// '';




//
//
module.exports =
// ' <div id="Archive">'+
// '  <div class="container">'+
// '    <h1>Search the Archives</h1>'+
// '  </div>'+
'  <div class="creek-broadcasts-filter-toolbar">'+
'    <form id="creek-archives-search-form">'+
'      <div class="creek-inputs-container">'+
'        <div class="creek-input-block">'+
'          <select type="text" id="creek-broadcasts-program" class="creek-input">'+
'            <option value="">Show Name</option>'+
'            <option value="">I don\'t know</option>'+
// '            <option v-repeat="program in programs" v-attr="value: program.id" v-text="program.title"></option>'+
'          </select>'+
'        </div>'+
'        <div class="creek-input-block">'+
'          <select type="text" id="broadcasts-show-ategory" class="creek-input">'+
'            <option value="">Category</option>'+
'            <option value="">I don\'t know</option>'+
// '            <option v-repeat="category in categories" v-attr="value: category.id" v-text="category.title"></option>'+
'          </select>'+
'        </div>'+
'        <div class="creek-input-block">'+
'          <input type="text" id="broadcasts-date-start" class="creek-input" placeholder="Date" />'+
'          <!-- <input type="hidden" id="broadcasts-date-end" /> -->'+
'        </div>'+
'        <div class="creek-input-block">'+
'          <input type="text" id="broadcasts-search-box" class="creek-input" placeholder="Keywords..." />'+
'        </div>'+
'      </div>'+
'    </form>'+
// '<!-- ";
'    <div class="submit">'+
'      <button type="button" id="broadcasts-search-button">Find Archives</button>'+
'    </div>'+
// ' -->'+
'  </div>'+
// '  <div >'+
// '    <div class="creek-broadcasts-container">'+
'      <div class="creek-container creek-broadcasts">'+
'        <div class="creek-broadcast" v-for="broadcast in broadcasts" :data-url=" \'/broadcasts/\'+broadcast.id">'+
'            <a class="creek-image" :href="\'/broadcasts/\'+broadcast.id"><img :src="broadcast.Image ? broadcast.Image.url_sm : broadcast.Show.Image.url_sm" /></a>'+
// '            <h3><span class="archive-title"><span v-text="broadcast.Show.title"></span> - <span v-text="broadcast.title"></span></span></h3>'+
// '            <h3><span class="creek-archive-title"><span v-text="broadcast.Show.title"></span></span></h3>'+
'            <div class="creek-info">'+
'              <a class="creek-broadcast-title" v-text="broadcast.title" :href="\'/broadcasts/view/\'+broadcast.id"></a>'+
'              <a class="creek-show-title" v-text="broadcast.Show.title" :href="\'/shows/\'+broadcast.Show.short_name"></a>'+
'              <span class="creek-date-time" v-text="broadcast.start | moment \'dddd, MMMM Do YYYY, h:mma\'"></span>'+
'            </div>'+
// '            <div class="creek-item-description">'+
// // '              <!-- <a class="read-more" :href="\'/broadcasts/\'+broadcast.id ">Read more</a> -->'+
// '              <span v-text="broadcast.text"></span>'+
// '            </div>'+
// '            <div class="creek-social-buttons">'+
// '              <span class="creek-social-button creek-social-button-facebook" v-attr="data-url: Creek.base_url+\'/broadcasts/\'+broadcast.id " href="#"><i class="fa fa-facebook"></i> Share</span>'+
// '              <span class="creek-social-button creek-social-button-twitter" v-attr="data-url: Creek.base_url+\'/broadcasts/\'+broadcast.id " href="#"><i class="fa fa-twitter"></i> Tweet</span>'+
// '              <span class="creek-social-button creek-social-button-email" href="#" v-attr="data-url: Creek.base_url"><i class="fa fa-envelope"></i> Email</span>'+
// '            </div>'+
'        </div>'+
'      </div>'+
// '    </div>'+
// '  </div>'+
// '</div>';
'';
