$ = require('jquery');

module.exports.init = function(Creek){

  var volumeOpen = false;
  var volumeDragging = false;

  //Functions:

  var hideVolume = function(){
    Creek.debug.log("Closing volume tool.")
    volumeOpen = false;
    // $(".creek-volume-button").addClass('creek-volume-open-button');
    // $(".creek-volume-button").removeClass('creek-volume-close-button');
    // $("#creek-audio-volume-tool").removeClass('creek-audio-volume-visible');
    $("body").removeClass('creek-audio-volume-visible');
  }

  var showVolume = function(){
    Creek.debug.log("Opening volume tool.")
    volumeOpen = true;
    // $(".creek-volume-button").removeClass('creek-volume-open-button');
    // $(".creek-volume-button").addClass('creek-volume-close-button');
    // $("#creek-audio-volume-tool").addClass('creek-audio-volume-visible');
    $("body").addClass('creek-audio-volume-visible');
  }

  var dragVolume = function(event, element){
    var posY = $(element).offset().top;
    var clickWhere = event.pageY - posY;

    var theVolume = 1 - (clickWhere / 100);
    var fillerHeight = (100 * theVolume) + 'px';

    Creek.audio.volume = theVolume*100;

    $('#creek-audio-volume-tool .bar .filler').css('height', fillerHeight);
    soundManager.setVolume(Creek.audio.volume);
  }

  //Stuff to do after page loads:
  $(function(){

    //Handle clicking volume button (open or close volume)
    $(document).on('click', '.creek-audio-volume-button-open', function(){
      showVolume();
    })

    $('#creek-audio-volume-tool .bar').on('mousedown', function(e) {
      volumeDragging = true;
    });
    $('#creek-audio-volume-tool .bar').on('mouseup', function(e) {
      volumeDragging = false;
    });
    $('#creek-audio-volume-tool .bar').on('mousemove mousedown', function(e) {
      if(volumeDragging){
       dragVolume(event, $(this))
      }
    });

    //Close volume if clicking outside.
    $(document).mouseup(function (e)
    {

      // Creek.debug.log("Volume clicked: "+$(e.target).outerHtml)

      var container = $("#creek-audio-volume-tool");

      if(
          volumeOpen // first, if volume is open
          // && $(this).closest(".creek-volume-button").length < 1 // volume button is handled separately.
          && !container.is(e.target) // if the target of the click isn't the container...
          && container.has(e.target).length === 0) // ... nor a descendant of the container
      {
        Creek.debug.log("Volume: clicked outside.")
        hideVolume();
      }

    });

  })

}
