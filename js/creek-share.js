
var $ = require('jquery');

module.exports.handlers = {
  fb: function(url) {
    var url = url || window.location;
    var winWidth = 520;
    var winHeight = 350;
    var winTop = (screen.height / 2) - (winWidth / 2);
    var winLeft = (screen.width / 2) - (winHeight / 2);
    window.open('http://www.facebook.com/sharer.php?s=100&p[url]=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
  },
  tw: function(url) {
    var url = url || window.location,
        width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    url_encoded = encodeURIComponent(url);
    window.open('http://twitter.com/share?url='+url_encoded, 'Twitter', opts);
  }
}

module.exports.init = function(Creek){

  /*---------------------

  Share buttons

  ... and setting them up.

  */

  var share_clicked = function(type, element){
    var url = element.attr("data-url") || window.location;
    // console.log(element.attr("data-url"));
    module.exports.handlers[type](url);
  }

  //Broadcast button
  $(document).on("click", "#Broadcasts .broadcast", function(){
    var url = $(this).attr("data-url");
    // Creek.singlePage.methods.loadContent({url: url});
  })

  //Activate social buttons
  $(document).on("click", ".creek-social-button-facebook", function(){
    share_clicked("fb", $(this));
  })
  $(document).on("click", ".creek-social-button-twitter", function(){
    share_clicked("tw", $(this));
  })


  //Email share box

  var html_email_box =
  '<div class="creek-social-email-box creek-hide">'+
    '<p>Copy and paste this link into an email:</p>'+
    '<input type="text" id="creek-email-link" value="" onclick="this.select()" />'+
    '<span class="creek-close">Close</span>'+
  '</div>'

  function email_button_render(){
    $('body').append(html_email_box);
    // console.log(html_email_box);
  }

  function email_button_show(element){

    var top = element.offset().top - 3 + "px";
    var left = element.offset().left -3 + "px";
    // console.log(element.offset())

    if(element.attr("data-url")){
      $("#creek-email-link").val(element.attr("data-url")).focus();
    }else{
      $("#creek-email-link").val(window.location).focus();
    }

    $(".creek-social-email-box")
      .css("top", top)
      .css("left", left)
      .removeClass("creek-hide");

  }

  function email_button_hide(element){
    $(".creek-social-email-box").addClass("creek-hide");
  }

  $(document).on('click', '.creek-social-button-email', function() {
    // console.log("hey");
    email_button_show($(this));
  })
  $(document).on('click', '.creek-social-email-box .creek-close', function() {
    email_button_hide($(this));
  })
  $(document).on("focus", '#creek-email-link', function(){
    $(this).focus();
  });

  $(function(){
    email_button_render();
  })

  return module.exports.handlers;

}
