/*

WordPress Shim

This is only here as a placeholder to
keep track of the important variables.

*/

//Safely get settings, in case they aren't defined in WordPress yet:
wordpress_options.ssl_enable = wordpress_options.ssl_enable || false;
wordpress_options.station_id = wordpress_options.station_id || null;
wordpress_options.station_domain = wordpress_options.station_domain || null;
wordpress_options.persistence_enable = wordpress_options.persistence_enable || false;
wordpress_options.persistence_type = wordpress_options.persistence_type || null;
wordpress_options.persistence_container = wordpress_options.persistence_container || null;
wordpress_options.css_custom = wordpress_options.css_custom || null;
wordpress_options.javascript_custom = wordpress_options.javascript_custom || null;
wordpress_options.debug = wordpress_options.css_custom || null;

//Miscellaneous shim:
// !!! Where is this used? KWMR?
var wpntView = function(){};

//Add custom CSS and JS to the <head>
function creek_wp_custom_head_element(creek_wp_option, creek_c_tag, creek_c_type){

  var element = document.createElement(creek_c_tag);
  var content = wordpress_options[creek_wp_option];
  element.type = creek_c_type;
  element.innerHTML = content;
  document.getElementsByTagName('head')[0].appendChild(element);

  if(wordpress_options.debug){
    console.log("Adding <"+creek_c_tag+"> to <head> with this content:");
    console.log(content);
  }

}
creek_wp_custom_head_element("css_custom", "style", "text/css");
creek_wp_custom_head_element("javascript_custom", "script", "text/javascript");

//Initialize Creek
Creek.init({

  // Use variables from Creek's WordPress plugin settings.
  domain: wordpress_options.station_domain,
  id: wordpress_options.station_id,

  ssl: false,
  // ssl: wordpress_options.enable_ssl,

  debug: 1,
  // debug: wordpress_options.debug,

  shows: {
    // schedule: {
    //   enable: false
    // }
    prefix: "/shows/"
  },
  router: {
    /*
    Don't use JS router and schedule pages, since the point of this plugin is to have server-side PHP routing:
    */
    enable: false,
    prefix: "/"
  },
  persist: {
    enable: wordpress_options.persistence_enable,
    type: wordpress_options.persistence_type,
    settings: {
      singlepage: {
        container: wordpress_options.persistence_container
      },
      iframe: {
        mainElement: "#creek-audio-toolbar"
      },
    },
  }

})
