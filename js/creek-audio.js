/*

See Creek theme documentation for how to use this player:
http://help.creek.fm/

TODO:

- Call pre-rolls "Ads" or "messages" and give them more metadata, esp. option to be preroll or something else:
  { ads: [ {type: "preroll", id: "safe-driving-intro2", url: "http://..."}, ... ] }

- Change Last.fm art URL to Creek's album art CDN.

- Only add the player elements if this is enabled. Otherwise station has its own player HTML.

- Fix all the vars related to album art.

*/

var $ = require('jquery');
var jQuery = $;
var soundManager = require('../node_modules/soundmanager2/script/soundmanager2-nodebug-jsmin.js').soundManager;


module.exports.init = function(Creek){

  Creek.audio.stream = {};

  //Initialize SoundManager2
  soundManager.url = Creek.audio.soundManager2_url || "/";
  soundManager.flashVersion = 9;
  soundManager.preferFlash = false; // for visualization effects
  soundManager.useHighPerformance = true; // keep flash on screen, boost performance
  soundManager.wmode = 'transparent'; // transparent SWF, if possible
  soundManager.useFastPolling = true; // increased JS callback frequency

  //!!! Album art
  // var lastfm_art_url = "http://userserve-ak.last.fm/serve/64s/";
  // var album_art_previous = "";

  //!!! TODO: selective-async:
  // Selectively include this template only if toolbar is enabled.
  Creek.audio.toolbar = {

    html: Creek.util.fixLinks(require('./templates/audio-toolbar.js')),

    buildHtml: function(){

      //Create mobile audio player toolbar
      $(Creek.data.audio.toolbar.container).append(Creek.audio.toolbar.html);

      //Add spacer to make sure that the bottom of the page
      $("#content").append('<div id="creek-audio-toolbar-spacer">&nbsp;</div>');

      Creek.audio.util.resetSpacerHeight();

    }

  };


  Creek.audio.stream.setUrl = function(streamUrl, restart){

    //Set stream data
    // if(stream.url){
    //   Creek.debug.log('Setting stream URL to: '+Creek.audio.stream.currentUrl);
    // }

    restart = restart || false;

    Creek.audio.stream.currentUrl = streamUrl;

    console.log(Creek.audio.stream.currentUrl);

    if(restart){
      Creek.audio.stop();
      // http://stackoverflow.com/questions/36803176/how-to-prevent-the-play-request-was-interrupted-by-a-call-to-pause-error
      // Delay play() by 150 ms
      var waitTime = 150;
      setTimeout(function () {
        Creek.audio.play();
      }, waitTime);

    }

  };

  Creek.audio.stream.setUrlFromStationApi = function(data){

    console.log("YEPPPPP");
    console.log(data.streams)
    console.log(Creek.data.stream)

    var stream = {url: ''};
    if(typeof data.streams != 'undefined' && typeof Creek.data.stream == 'undefined'){
      //Get stream address from streams API data: either "website" or "main"
      stream = data.streams.website || data.streams.main;
      Creek.audio.stream.currentUrl = stream.url;
      console.log("THE STREAM IS: "+Creek.audio.stream.currentUrl);
    }
    //Allow init shortcut: "Creek.init({ ... stream: { url: "http://..." } })"
    else if(typeof Creek.data.stream != 'undefined') {
      stream.url = Creek.data.stream.url;
      Creek.debug.log("Custom stream: "+Creek.data.stream.url);
    }

    //Add stream to the default object as well
    Creek.audio.stream.setUrl(stream.url);
    // Creek.data.streams.url = stream.url;

  };

  Creek.audio.play = function(e){

    //Stop all previous sounds (like bar-ui)
    // soundManager.stopAll();

    Creek.debug.log('Playing...');

    if(!Creek.audio.selected){
        Creek.debug.log('Attempted to play, but stream not set.');
        return;
    }

    if(Creek.audio.state === "initializing"){
        Creek.debug.log('Attempted to play, but SM2 has not been initialized.');
        return;
    }

    //Play file if file is selected
    if(Creek.audio.selected.type == 'file'){

      soundManager.resumeAll();

      // !!! TODO: Need a better way of getting the actual bar-ui ID
      // if(typeof window.sm2BarPlayers[0] != "undefined"){
      //   window.sm2BarPlayers[0].actions.play()
      // }

      // window.sm2BarPlayer.actions.play()

      Creek.debug.log('Playing file again.');

    }
    else{

      Creek.debug.log('Playing live stream again.');

      console.log(Creek.audio.stream.currentUrl);

      Creek.audio.player = soundManager.createSound({
       // optional id, for getSoundById() look-ups etc. If omitted, an id will be generated.
       id: 'creek-audio-player-live',
       url: Creek.audio.stream.currentUrl,
       volume: 100,
       // autoPlay: true,
      //  whileloading: function() { console.log(this.id + ' is loading'); }
      });

      if(Creek.audio.player && Creek.audio.player.play){
          Creek.audio.player.play({
              onplay: function() {
                  Creek.audio.setState.loading();
              },
              onpause: function() {
                  Creek.audio.setState.paused();
              },
              onstop: function() {
                  Creek.audio.setState.stopped();
              },
              onerror: function() {
                  Creek.audio.setState.error();
              },
              onload: function(){
                  Creek.audio.setState.playing();
              }
          });
      }
      // createSound may also return 'false'
      else{
          Creek.debug.log('SM2 createSound() failed');
      }
    }

    // Creek.debug.log('Initializing stream: '+ Creek.streamPlayer.streamUrl);

    // Set up audio
    //
    // If preroll enabled, then play preroll first:
    // if(typeof Creek.streamPlayer.prerolls.enable != 'undefined' && Creek.streamPlayer.prerolls.enable && !Creek.util.cookies.read('preroll')){

    //   Creek.debug.log(getRandomPreroll());

    //   Creek.debug.log('Starting stream (with pre-roll).')

    //   Creek.util.cookies.create('preroll', 1, Creek.streamPlayer.preroll.timeBeforeNext);

    // }
    // else{

      // Creek.debug.log('Starting stream.')

      // Creek.util.cookies.delete('preroll');

    // }

    //Add loading class to document body (removed when sound starts)
    // $("body").addClass('creek-audio-loading');

    // Creek.audio.setState.playing();

    Creek.audio.analytics.startListening();

  };


  Creek.audio.pause = function(e){

    if(!Creek.audio.selected){
        Creek.debug.log('Attempt to pause, but stream not set');
        return;
    }

    soundManager.pauseAll();
    // Creek.audio.player.stop();

    //Only unload if it's the live stream
    if(Creek.audio.selected.type == 'live'){
      if(Creek.audio.player.loaded){
        Creek.audio.player.unload();
      }
    }

    // Creek.audio.setState.paused();

    //Send Google Analytics event (stop)
    if(typeof ga != 'undefined'){
      ga('send', 'event', 'button', 'stop', 'stream player');
    }

  };

  Creek.audio.stop = function(e){

    if(!Creek.audio.selected){
        Creek.debug.log('Attempt to stop, but stream not set');
        return;
    }

    soundManager.pauseAll();
    // Creek.audio.player.stop();

    //Destroy if it's the live stream
    if(Creek.audio.selected.type == 'live'){
      if(Creek.audio.player.loaded){
        Creek.audio.player.destruct();
      }
    }

    //!!! This is already called by SM2 events...
    // Creek.audio.setState.stopped();

  };


  Creek.audio.toggle = function(){

    var before = Creek.audio.state+""; //add empty string for cloning

    //If playing, then pause audio
    if(Creek.audio.state == "playing"){
      Creek.audio.pause();
    }
    //For all other states (paused, stopped, errored) play audio
    else {
      Creek.audio.play();
    }

    var after = Creek.audio.state+""; //add  empty string for cloning

    Creek.debug.log("Toggling audio from "+before+" to "+after+".");

  };

  //Do functions that should happen after page is loaded.
  $(function(){

    //!!!Only add the player elements if this is indeed enabled.
    //
    // !!! TODO: make sure that this is actually working and NECESSARY too
    //
    // console.log(Creek.audio)

    if(Creek.data.audio.toolbar.enable){
      Creek.audio.toolbar.buildHtml();
    }

    //Switch back to stream when creek-switch-to-live is clicked
    $('body').on('click', '.creek-audio-switch-to-live', function(e){
      Creek.debug.log('Clicked switch-to-live.');
      Creek.audio.switchToLive({originURL:null, title: null});
    });

    //Play audio when play button is pressed
    $('body').on('click', '.creek-audio-play-button', function(e){
      Creek.audio.play(e);
    });

    // TODO: This should be pause, since it's actually pausing.
    //Stop stream when stop button is pressed
    $('body').on('click', '.creek-audio-pause-button', function(e){
      Creek.audio.pause(e);
    });

    // //Handle post-pre-roll and post-whatever:
    // // Play stream after first audio file ends (if it does end at all)
    // $("#creek-audio-jp").bind($.jPlayer.event.ended, function(event) {
    //   $(this)
    //     .jPlayer("clearMedia")
    //     .jPlayer("setMedia", { mp3: Creek.streamPlayer.streamUrl })
    //     // .jPlayer("play")
    // });

  });



  // Preroll advertisement functionality
  // function checkPrerollEnabled(){
  //   if(typeof Creek.audio.prerolls.enable != 'undefined' && Creek.audio.prerolls.enable && !Cookies.read('preroll')){
  //     return true;
  //   }else{
  //     return false;
  //   }
  // }
  // function getRandomPreroll(){
  //   var pr_count = Creek.audio.prerolls.urls.length;
  //   var pr_index = Math.floor(Math.random() * (pr_count - 0)) + 0;
  //   return Creek.audio.prerolls.urls[pr_index];
  // }


  //Initialize Volume tool
  $(function(){
    var volume_init = require('./creek-audio-volume.js').init(Creek);
  });


  // Creek.sm2_bar.build();

  return Creek.audio;

  // } // end check for whether Creek is defined at all

} //End global function container
