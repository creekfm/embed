
var io = require('socket.io-client');
var $ = require('jquery');


//Load the station API data to get the stream.
var initialLoad = function(Creek, data){

  Creek.debug.log("Loading Station API and stream addresses.");

  $.getJSON(Creek.data.url_prefix+"/api/station?callback=?", function(data){

    Creek.debug.log("Received Station API. Activating.")
    Creek.debug.log(data)

    activateStation(Creek, data);

  })

}

var activateStation = function(Creek, data){

  Creek.debug.log("Stream address ready:")
  Creek.debug.log(data, 1);

  Creek.audio.stream.setUrlFromStationApi(data);

  //Set preroll data
  // if(typeof Creek.data.station.prerolls != 'undefined'){
  //   Creek.audio.prerolls = Creek.data.station.prerolls;
  // }

  Creek.audio.showAudioToolbar(Creek);

  Creek.audio.activateInitialLiveAudio(Creek);

}

var updateBroadcasting = function(Creek, data){

  Creek.debug.log(data);

  if(!data){
    data = { now: { title: "", short_name: "" }, next: {}, Track: {} };
  }

  Creek.data.broadcasting = data;

  //Don't update UI or Creek.audio.selected if playing an audio file.
  if(Creek.audio.selected && Creek.audio.selected.type == 'file'){
    return;
  }

  var audio_data = data;
  audio_data.now = data.now || { title:"", short_name: "" };
  // audio_data.now = data.now || { title:Creek.data.station.title };
  audio_data.next = data.next || {};

  //Set up data for current show
  audio_data.type = "live";
  audio_data.title = audio_data.now.title || null;
  audio_data.originURL = Creek.data.shows.prefix + audio_data.now.short_name;

  // console.log("XXXXCXCXCXZVCXVCZXVCX");
  // console.log(Creek.data.shows.prefix);
  // console.log(audio_data);

  if(Creek.data.demo && !audio_data.Track)
    audio_data.Track = {
      artist: "Dr. Demo",
      title: "Demo Song",
      album: "The Demo Album",
      label: "Demos, Inc."
    }

  // console.log(Creek.audio);
  Creek.audio.update(audio_data);

  Creek.audio.util.resetSpacerHeight();

}


module.exports.init = function(Creek){

  initialLoad(Creek);

  //Set up URL based on HTTPS / HTTP
  // if(Creek.data.station.ssl){
  //   var socket_url = 'https://'+Creek.data.station.domain+':3443/';
  // }
  // else{
  //   var socket_url = 'http://'+Creek.data.station.domain+':3305/';
  // }

  //Always use HTTPS for Socket.io
  var socket_url = 'https://'+Creek.data.station.domain+':3443/';

  var socket = io(socket_url);

  Creek.debug.log("Connecting to Live API at: "+socket_url);


  //Handle when the station connects with socket.io
  socket.on('api connection', function(data){

    Creek.debug.log('Connected to Live API.');

    //Select the station, based on ID supplied in Creek.init({...})
    socket.emit('station select', Creek.data.station.domain, function(){

      Creek.debug.log("Selected station: "+Creek.data.station.domain);

    });

  })

  //Initialize stream player after it's connected.
  // socket.on('api station', function(data){
  //   if(Creek.data.liveApi.enable){
  //     Creek.debug.log("OH YEAH:")
  //     Creek.debug.log(data, 1);
  //     activateStation(Creek, data);
  //   }
  // })

  //Update Creek data and UI when broadcasting data is updated.
  socket.on('api broadcasting', function(data){
    if(Creek.data.liveApi.enable){
      updateBroadcasting(Creek, data);
    }
  })

  //Error handling
  socket.on('connect_error', function(err){
    Creek.debug.log("ERROR: Could not connect: "+err);
    return false;
  })
  socket.on('connect_timeout', function(err){
    Creek.debug.log("ERROR: Connection timed out: "+err)
    return false;
  })
  socket.on('reconnect_error', function(err){
    Creek.debug.log("ERROR: Could not reconnect: "+err)
    return false;
  })
  socket.on('reconnect_failed', function(err){
    Creek.debug.log("ERROR: Too many failed reconnection attempts: "+err)
    return false;
  })

}
