var jQuery = require('jquery');

module.exports.defaults = {

  //Demo mode displays demo data for:
  // - Track
  // ...when there isn't any.
  demo: false,

  debug: false,

  //Information about the station
  station: {

    //Uses document's domain by default.
    domain: document.domain,

    //!!! id should be unnecessary since Live API should use domain
    id: null,

    title: "",

    //Enable HTTPS by default
    ssl: true

  },

  //The currently-broadcasting audio content
  broadcasting: {
    now: null,
    next: null,
    upcoming: null,
    Track: null
  },

  streams: {

  },

  liveApi: {

    //Connect to the Live API with Socket.IO
    enable: true

  },

  audio: {
    toolbar: {

      //Whether enable the toolbar and its functions
      enable: true,

      showImageSize: "sm",

      //Custom HTML for the toolbar.
      // html: null  //Nevermind, just allow them to use their own things.

      //Allows you to inject the toolbar into a container.
      // (Might only be useful for the Creek demo, though!)
      container: "body"
      // container: ".creek-audio-toolbar-container"

    },
    players: {
      backgroundColor: "rgba(0,0,0,.2)",
      textLightOrDark: "dark"
    }
  },

  //Options for the Creek Shows framework
  shows: {

    data: null,

    container: "#Creek-Shows",

    //Prefix for links:

    //What about /broadcasts ?

    //With JS router (injects shows/broadcast page content into container box)
    // prefix: "#!/shows/",

    //Replaced by JS router if not WordPress or custom-defined.
    prefix: "/shows/",

    //Cache for API data
    schedule: {
      data: null
    },
    all: {
      data: null
    },

  },

  //The built-in router that creates a dedicated Shows page by injecting page content for shows, broadcasts, and hosts directly into the same container box as the schedule.
  router: {

    enable: true,

    //Enables or disables updating page titles
    enableTitles: true,

    //Not usually necessary since all paths are
    //relative and start with !#/
    baseUrlOrPath: "/shows.html",
    // baseUrlOrPath: "/",  // <------------------- WordPress

    loadingHtml: '<i class="creek-loading-icon fa fa-spinner fa-spin fa-2x"></i>',

    //Allows you to call them "programs" or
    // "shows" or "content" or whatever.
    showsName: "shows",

    prefix: "#!/",
    // prefix: "/shows/" // <------------------- WordPress

    linkReplacements: {
      "/shows/": "#!/shows/",
      "/broadcasts/view/": "#!/broadcasts/",
      "/broadcasts/": "#!/broadcasts/",
      "/profiles/": "#!/profiles/",
      // "#play": "javascript:",
      // "#volume": "javascript:"
    }

  },

  persist: {

    /*
    Documentation notes:
    - Use .creek-persist-link to make links target the iframe rather than the window.
    - Use .creek-persist to make that element get persisted, rather than deleted when the page loads.
    */

    type: "singlepage",

    enable: false,

    settings: {

      //Persistence through Magic Iframe:
      // Places everything into an iframe, except for the main audio toolbar, <head>, <body>, and a few user-defined elements.
      iframe: {

        mainElement: "#creek-audio-toolbar"

      },

      //Persistence through Content Injection:
      // This persistence technique intercepts link clicks, and injects the intended page content into the main page container. This, rather than refreshing the page and breaking any active audio players.
      singlepage: {

        //domain:
        // The domain of the website. If a link contains this domain, then follow normal content injection behavior, rather than treating it like an external link that should send the browser entirely to the URL. (Example: if a link href has a full URL like "http://station-name.org/about" rather than just "/about")
        domain: document.domain,

        //EXPERIMENTAL FEATURE
        //loadNewAssets:
        // Load new CSS / JS: Iterate over the JS and CSS objects in the intended page, find ones that aren't already on the page, and add them to the <head> or <body> of the document.
        loadNewAssets: false,

        //container:
        // ID of container element. Leave out the: #
        container: "#creek-persist-container",

        // Loading indicator settings:
        // How long to wait before showing the loading indicator.
        loadingIndicatorDelay: 800,
        // Whether or not the loading indicator is currently active.
        loadingStatus: false,

        //onLoading: useful for doing things while hte page is loading, especially displaying a loading bar.
        onLoading: function(){ },

        //onLoading: useful for doing things when the page is done loading, especially for hiding a loading bar.
        onLoaded: function(){ },

        //Ran when the page is done loading.
        onComplete: function(){ },

        //onCompleteArray:
        // An array of functions that are all called when the page is done loading, allowing you to push() functions to this array. Useful in cases where there are multiple callbacks that must be fired, and these callbacks are located in different scripts loaded at different times in the page's lifetime.
        onCompleteArray: [],

        //ignorePaths:
        // If these strings are found in the link's 'href' attribute, then clicking on them will fully refresh the page, bypassing the persistence function (where otherwise the intended page content is injected into the DOM).
        ignorePaths: [
          'wp-admin'
          // '/programmer-portal/',
          // '/programmer-portal',
          // '/quarterly-issues/',
          // '/quarterly-issues'
        ]

      }
    }
  }

}


//Combine defaults with any options provided in Creek.init({...})
module.exports.init = function(config){

  var defaults = module.exports.defaults;

  //----------------------------------
  // Set up shortcut variables:
  //----------------------------------
  if(typeof config.domain != 'undefined'){
    // config = jQuery.extend(true, {}, unfilteredData, {data: { station: { domain { config.domain } } });
    defaults.station.domain = config.domain;
  }
  if(typeof config.id != 'undefined'){
    // config = jQuery.extend(true, {}, unfilteredData, {data: { station: config.id } });
    defaults.station.id = config.id;
  }
  if(typeof config.stream != 'undefined'){
    // config = jQuery.extend(true, {}, unfilteredData, {data: { station: config.id } });
    defaults.streams.main = { url: config.stream.url };
  }
  if(typeof config.ssl != 'undefined'){
    // config = jQuery.extend(true, {}, unfilteredData, {data: { station: { domain { config.domain } } });
    defaults.station.ssl = config.ssl;
  }

  //----------------------------------
  // Combine data:
  //----------------------------------
  var newDataPlusDefaults = jQuery.extend(true, {}, defaults, config);

  //----------------------------------
  // Set up default fixes:
  //----------------------------------
  if(newDataPlusDefaults.router.enable){

    newDataPlusDefaults.shows.prefix = newDataPlusDefaults.router.prefix;

  }

  //Set up URL prefix for schedule and the archives
  newDataPlusDefaults.schema = newDataPlusDefaults.station.ssl ? "https" : "http";
  newDataPlusDefaults.url_prefix = newDataPlusDefaults.schema+"://"+newDataPlusDefaults.station.domain;

  // Debug output:
  // console.log(newDataPlusDefaults);
  // console.log(config);

  return newDataPlusDefaults;

}
