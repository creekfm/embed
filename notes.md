
# Notes

### Audio Player

- I need to use the simplest tag for the default audio tag that's replaced by Creek's "audio player build" function. Should I use the <audio> tag? It makes sense as a non-JS fallback. It's similar to how an embedded Tweet uses a blockquote.

- And if so, should I use "src" and "data-title"? What other data would be appropriate? media-id and station-id, in order to get more metadata?

- What about the "flash of unstyled content" before the JS loads that replaces the audio files? Should I just use:

    ```
    <div class="creek-audio-player">
      <a href="url-to-file.mp3">Title</a><br>
    </div>
    ```

    Or:

    ```
    <ul class="creek-audio-player">
      <li data-url="https://creekfm-test1.objects.dreamhost.com/audio/lesser-labyrinths/lesser-labyrinths_2015-11-16_01-00-00.mp3">lesser labyrinths – November 16, 2015</li>
    </ul>
    ```

<br> so that lines happen even with default-inline elements.


#### However:

The audio


### Framework agnostic

You should be able to remove the Persistence script and build a site entirely in Vue, Ember, Angular, React, etc.

Make the live-api data available in Creek.data.

How would someone typically try to push this Creek.data into their JS framework?


### Too big

- Use Gzip.
- Use caching settings on the CDN.
- Use only specific font-awesome icons.
- Vue should only be included only if page is using the schedule.
- I am only using a few jQuery functions, so most of those can go.
- Use uglify settings to remove unused code.
- remove moment by having times already formatted in JSON.
- basicPersistence can go away if they are using Vue + router or Ember.
- Just use polling instead of socket.io? No, that would strain the servers.


#### Questions:

Is there a way to load only the required libraries? It doesn't seem like so, since there is no good way to tell if the page already includes, for example, moment.js.



# Build

browserify creek-build.js -d -t browserify-css -p [minifyify --map creek.map.json --output creek.map.json] -o creek.js

watchify creek-build.js -d -t browserify-css -p [minifyify --map creek.map.json --output creek.map.json] -o creek.js
