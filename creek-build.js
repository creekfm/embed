/*

  Creek: Initialize, with modules.

*/

var Creek = {};

//jQuery, for the element selector, and for grabbing remote JSONP content.
var $ = require('jquery');
var jQuery = $;

/*
 Vue, moment, and vue-moment are only included in:
 - creek-schedule.js
*/

/* ------------------------------------
 Include CSS
*/

var css = require('./css/creek.css');

//FontAwesome
// Modified CSS with Absolute-URLs
// !!! This should be replaced with individual icons.
var css_fontawesome = require('./css/font-awesome.css');

//SM2 Bar UI
// Modified CSS with Absolute-URLs
var css_sm2_bar_ui = require('./lib/sm2/bar-ui/css/bar-ui.css');


/* ------------------------------------

  Initiate JavaScript objects for Creek

*/

/*

  TODO:
  - Remove jQuery dependency somehow?
  - Handle the option to enable/disable stream toolbar

*/


//----------------------
// Create init function
//----------------------

Creek.init = function(config){

  //Prepare data based on incoming config data
  Creek.data = require('./js/creek-data.js').init(config);

  Creek.page = {
    reset: function(){ },
  }

  Creek.debug = require('./js/creek-debug.js').init(config, Creek);

  Creek.util = require('./js/creek-util.js').init(Creek);

  Creek.content = require('./js/creek-content.js').init(Creek);

  //Audio Part 1:
  // Set up shared objects.
  Creek.audio = require('./js/creek-audio-lib.js').init(Creek);

  //Audio Part 2:
  // Requires functions from creek-audio-lib.js
  Creek.audio = require('./js/creek-audio.js').init(Creek);

  //Audio Part 3:
  // Requires Creek.audio
  Creek.sm2_bar = require('./js/sm2-bar-ui-creek.js').init(Creek);

  //The Socket.io-based API interface that delivers /api/broadcasting and other events.
  Creek.liveApi = require('./js/creek-live-api.js').init(Creek);

  // Requires Creek.audio and sm2_bar
  if(Creek.data.persist.enable){
    if(!Creek.data.persist.type){
      console.error("Creek: Since persistence is enabled, you must specify a persistence type.")
    }
    if(Creek.data.persist.type == "iframe"){
      Creek.data.persist.instance = require('./js/creek-persist-iframe.js').init(Creek);
    }
    if(Creek.data.persist.type == "singlepage"){
      Creek.data.persist.instance = require('./js/creek-persist-singlepage.js').init(Creek);
    }
  }else{
    Creek.data.persist.instance = null;
  }

  //Share functions for social media.
  Creek.share = require('./js/creek-share.js').init(Creek); //No "Creek" needed.

  //Shows, schedule, broadcasts, hosts + JS Router
  Creek.shows = require('./js/creek-shows.js').init(Creek);

  // The archives browser:
  // !!! Disabled until it works.
  // Creek.archives = require('./js/creek-archives.js').init(Creek);

  return true;

}

/* ------------------------------------------------------

  Expose Creek to global scope:

  Allows you to access Creek from outside of this Browserify bundle.

*/
window.Creek = Creek;
