#Creek Integration Library

This lets you embed Creek's radio tools on any website.

- Audio player for live stream and archives.
- Archives.
- Show schedules. Both gallery and full calendar
- Audio player persistence: audio keeps playing while user browses website.

## Building with browserify

```
cd wherever/you/put/embed

# If needed, install browserify and its tools:
npm install -g browserify watchify minifyify

# Run this while editing the JS library:
watchify creek-build.js -d -t browserify-css -p [minifyify --map creek.map.json --output creek.map.json] -o creek.js
```

### Quick Example

```
<!-- Creek JavaScript Toolkit -->
<script type="text/javascript" src="http://cdn.creek.fm/embed/0.7.4c-alpha/creek.js"></script>

<!-- Apply one of the built-in CSS themes -->
<link rel="stylesheet" href="http://cdn.creek.fm/embed/0.7.4-alpha/css/light1.css">

<!-- This is the main content element into which the Creek shows and schedule are inserted. -->
<div id="Creek-Shows"></div>

<!-- Initialize the Creek JavaScript Toolkit -->
<script type="text/javascript">
  Creek.init({
    domain: "kusf.creek.fm",
    id: "kusf",
    demo: true,
    audio: {
      toolbar: {
        enable: true
      }
    },
    station: {
      title: "KUSF"
    },
    debug: true,
    shows: {
      schedule: {
        enable: true
      }
    }
  })
</script>

```
